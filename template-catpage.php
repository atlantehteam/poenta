<?php

/* 
 * Template name: Category Feed Page
 *
 * This template enables you to display most popular post in two columns layout, or to select
 * which categories to display in this layout. 
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
global $avantgardia_global, $post; 

$post_id = $post->ID;

$slider_code = trim( get_post_meta( $post_id, 'avantgardia_page_two_column_slider_code', true ) );
$has_slider = !empty( $slider_code );
$slider_width  = get_post_meta( $post_id, 'avantgardia_page_two_column_slider_width', true );
$slider_top = 'full-width' == $slider_width || 'window-width' == $slider_width;

get_header(); ?>
	<?php if( $has_slider && $slider_top ) { 
		
		$slider = do_shortcode( $slider_code );
		
		if( strpos( $slider, 'fullscreen-container' ) ) {
			$slider_classes = 'is-fullscreen';
		}else{
			$slider_classes = '';
		}
		
		$slider_classes .= ' ' . $slider_width;
	?>
	<div class="page-slider-wrap full-width-slider <?php echo esc_attr( $slider_classes ); ?>"><?php echo do_shortcode( $slider_code ); ?></div>
	<?php } ?>	
	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix">
			<?php if( $has_slider && !$slider_top ) { ?>
			<div class="page-slider-wrap boxed-slider"><?php echo do_shortcode( $slider_code ); ?></div>
			<?php } ?>
			<?php if( !empty( $post->post_content ) ) { ?>
			<div class="entry-content clearfix">
			<?php do_action( 'avantgardia_after_entry_content_open' ); ?>
				<div class="single-post-content">
					<?php the_content(); ?>
				</div>				
			<?php do_action( 'avantgardia_after_entry_content_close' ); ?>
			</div><!-- .entry-content -->
			<?php } ?>
		<?php if ( have_posts() ) : ?>
			
			<div class="archive-header">
				<!-- <div class="archive-page-name"><?php avantgardia_e( 'Archive', 'newspeak' ); ?></div> -->				
				<div class="archive-type"><h1 class="cat-title"><?php the_title(); ?></h1></div>
			</div>
			<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
			
			<?php
			echo "<script>console.log('x');</script>";
			$template_type = get_post_meta( $post_id, 'avantgardia_template_archive_template', true );
			
			$query = array( 'paged' => avantgardia_get_page_number(),
						'post_type' => 'post',
						'orderby' => 'post_date',
						'order' => 'DESC',
						/*'posts_per_page' => get_option( 'posts_per_page' )*/
						'posts_per_page' => 10
				);
		/*	$num_posts = intval( trim( get_post_meta( $post_id, 'avantgardia_templata_archive_num_posts', true ) ) );
			if( !empty( $num_posts) && $num_posts > 0 ) {
				$query['posts_per_page'] = $num_posts;
			}*/
			$query['posts_per_page'] = 10;
			$cats = get_post_meta( $post_id, 'avantgardia_template_archive_categories', true );
			
			if( !empty( $cats ) ){
				$cats = implode( ',', $cats );
				$query['cat'] = $cats;
			}
			
			query_posts( $query );
			?>
			<div class="archive-posts-wrap post-content-wrapper">
			<?php 
									
			// Start the loop.
			while ( have_posts() ) : the_post();

				get_template_part( 'content/post-list/block', $template_type );
				
			// End the loop.
			endwhile;
			
			$page_pagination = get_post_meta( $post_id, 'avantgardia_templata_archive_pagination', true );
			$pagination = '';
			if( !empty( $page_pagination ) && $page_pagination != 'default' ) {
				$pagination = $page_pagination;
			}			
			// Previous/next page navigation.
			avantgardia_get_page_navigation( 'archive-load-more', $pagination );
							
			?></div><!-- .archive-posts-wrap --><?php 

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>

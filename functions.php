<?php 
/**
 * You can use this file to add your custom code, or override some functions from parent theme.
 */
require_once(get_stylesheet_directory().'/functions-overwrite.php');
require_once(get_stylesheet_directory().'/functions-bbpress.php');
require_once(get_stylesheet_directory().'/includes/functions-shortcodes.php');
require_once(get_stylesheet_directory().'/includes/site-scripts.php');

if ( ! function_exists( 'wpartisan_set_no_found_rows' ) ) :
 
    /**
     * Sets the 'no_found_rows' param to true.
     *
     * In the WP_Query class this stops the use of SQL_CALC_FOUND_ROWS in the
     * MySql query it generates.
     *
     * @param  WP_Query $wp_query The WP_Query instance. Passed by reference.
     * @return void
     */
    function wpartisan_set_no_found_rows( \WP_Query $wp_query ) {
 

        if ( $wp_query->is_main_query() ) {

            $wp_query->set( 'no_found_rows', true );
 
        }
    }
endif;
/*add_filter( 'pre_get_posts', 'wpartisan_set_no_found_rows', 10, 1 );*/


add_action('admin_init','optionscheck_change_santiziation', 100);
 
function optionscheck_change_santiziation() {
    remove_filter( 'of_sanitize_textarea', 'of_sanitize_textarea' );
    add_filter( 'of_sanitize_textarea', 'custom_sanitize_textarea' );
}
 
function custom_sanitize_textarea($input) {
    global $allowedposttags;
    $custom_allowedtags["embed"] = array(
      "src" => array(),
      "type" => array(),
      "allowfullscreen" => array(),
      "allowscriptaccess" => array(),
      "height" => array(),
          "width" => array()
      );
      $custom_allowedtags["script"] = array( "src" => array(),"async" => array());
	  $custom_allowedtags["ins"] = array("style" => array(), "data-ad-client" => array(), "data-ad-slot" => array(), "data-ad-format" => array());
      $custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
      $output = wp_kses( $input, $custom_allowedtags);
    return $output;
}

/*add_action( 'init', 'register_my_menu' );

function register_my_menu() {
    register_nav_menu( 'popup-menu', __( 'Popup Menu' ) );
}*/
function menu_function($atts, $content = null) {
extract(
shortcode_atts(
array( 'name' => null, ),
$atts
)
);
return wp_nav_menu(
array(
'menu' => $name,
'echo' => false
)
);
}
add_shortcode('menu', 'menu_function');
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

function wpb_move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields;
}
 
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'פוטר כתבה',
	  'id' => 'article-foot-sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
  )
);

function itc_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Share buttons', 'poenta' ),
		'id' => 'posts-social-buttons',
		'description' => __( 'Share buttons', 'poenta' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<span>',
		'after_title' => '</span>',
	) );

}

add_action( 'widgets_init', 'itc_widgets_init' );

add_action( 'after_wp_tiny_mce', function(){
	?>
	<script>
		var originalWpLink;
		// Ensure both TinyMCE, underscores and wpLink are initialized
		if ( typeof tinymce !== 'undefined' && typeof _ !== 'undefined' && typeof wpLink !== 'undefined' ) {
			// Ensure the #link-options div is present, because it's where we're appending our checkbox.
			if ( tinymce.$('#link-options').length ) {
				// Append our checkbox HTML to the #link-options div, which is already present in the DOM.
				tinymce.$('#link-options').append(<?php echo json_encode( '<div class="link-nofollow"><label><span></span><input type="checkbox" id="wp-link-nofollow" /> Add rel="follow" to link</label></div>' ); ?>);
				// Clone the original wpLink object so we retain access to some functions.
				originalWpLink = _.clone( wpLink );
				wpLink.addRelNofollow = tinymce.$('#wp-link-nofollow');
				// Override the original wpLink object to include our custom functions.
				wpLink = _.extend( wpLink, {
					/**
					 * Fetch attributes for the generated link based on
					 * the link editor form properties.
					 *
					 * In this case, we're calling the original getAttrs()
					 * function, and then including our own behavior.
					 */
					getAttrs: function() {
						var attrs = originalWpLink.getAttrs();
						attrs.rel = wpLink.addRelNofollow.prop( 'checked' ) ? 'follow' : false;
						return attrs;
					},
					/**
					 * Build the link's HTML based on attrs when inserting
					 * into the text editor.
					 *
					 * In this case, we're completely overriding the existing
					 * function.
					 */
					buildHtml: function( attrs ) {
						var html = '<a href="' + attrs.href + '"';
						if ( attrs.target ) {
							html += ' target="' + attrs.target + '"';
						}
						if ( attrs.rel ) {
							html += ' rel="' + attrs.rel + '"';
						}
						return html + '>';
					},
					/**
					 * Set the value of our checkbox based on the presence
					 * of the rel='nofollow' link attribute.
					 *
					 * In this case, we're calling the original mceRefresh()
					 * function, then including our own behavior
					 */
					mceRefresh: function( searchStr, text ) {
						originalWpLink.mceRefresh( searchStr, text );
						var editor = window.tinymce.get( window.wpActiveEditor )
						if ( typeof editor !== 'undefined' && ! editor.isHidden() ) {
							var linkNode = editor.dom.getParent( editor.selection.getNode(), 'a[href]' );
							if ( linkNode ) {
								wpLink.addRelNofollow.prop( 'checked', 'follow' === editor.dom.getAttrib( linkNode, 'rel' ) );
							}
						}
					}
				});
			}
		}
	</script>
	<style>
	#wp-link #link-options .link-nofollow {
		padding: 3px 0 0;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}
	#wp-link #link-options .link-nofollow label span {
		width: 83px;
	}
	.has-text-field #wp-link .query-results {
		top: 223px;
	}
	</style>
	<?php
});

// define the wpcf7_admin_warnings callback 
function ampforwp_post_after_design_elements_hook(  ) { 
global $avantgardia_global; 
ob_start();
	//echo do_shortcode('[addtoany buttons="facebook,twitter,google_plus"]');
    echo '
	<!-- AddToAny BEGIN -->
	<div class="a2a_kit a2a_kit_size_32 a2a_default_style" id="a2a_kit_c">
	<a class="a2a_button_facebook"></a>
	<a class="a2a_button_twitter"></a>
	<a class="a2a_button_google_plus"></a>
	<a class="a2a_button_email"></a>
	<a class="a2a_button_pinterest"></a>
	<a class="a2a_button_linkedin"></a>
	<a class="a2a_button_whatsapp"></a>
	</div>
	<script>
	//jQuery(document).ready(function() { setTimeout(function() {  $(".sp-cnt").prepend($(".a2a_kit").clone()) }); } , 1000);
	var itm = document.getElementById("a2a_kit_c");
	var cln = itm.cloneNode(true);
	eElement = document.getElementsByClassName("sp-cnt")[0];
	
	eElement.insertBefore(cln, eElement.firstChild);

	</script>
	<script async src="https://static.addtoany.com/menu/page.js"></script>
	<!-- AddToAny END -->
	<style>
	.s_stk.ss-ic {
		display: none;
	}
	.m-menu ul li.menu-item-has-children:after {
		content: "\e316";
	}

	aside.m-ctr {
		width: 100%;
	}
	.loop-wrapper {
		margin: 0;
	}
	.ss-ic { 
		display: none;
	}

	.a2a_kit {
		text-align: center;
		text-align-last: center;
	}

	.a2a_kit>a {
		margin: auto;
		display: inline-block;
		float: none;
	}
	</style>';
	ob_flush();
}; 
         
// add the action 
add_action( 'ampforwp_post_after_design_elements', 'ampforwp_post_after_design_elements_hook', 10, 0 ); 


// function order_posts_by_title( $query ) { 

// 	if (is_admin()) {
// 		return;
// 	}
//     $query->set( 'orderby', 'date' ); 
// 	$query->set( 'order', 'desc' ); 
// 	$query->set('ignore_sticky_posts', true);  
// 	//if ($_GET["q"]=="q") { print_r($query); }
// } 

// add_action( 'pre_get_posts', 'order_posts_by_title', 1001,1 );

function wpb_hook_utm() {
    ?>

         <script type="text/javascript">
                function getQueryVariable(variable)
                {
                var query = window.location.search.substring(1);
                var vars = query.split("&");
                for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
                }
                return(false);
                }    
                jQuery(document).ready(function() {
                jQuery('form').find('input.utm_source').each(function() {
                var a = getQueryVariable('utm_source');
                if(a){
                jQuery(this).val(a);
                }
                });
                jQuery('form').find('input.utm_medium').each(function() {
                var a = getQueryVariable('utm_medium');
                if(a){
                jQuery(this).val(a);
                }
                });
                jQuery('form').find('input.utm_campaign').each(function() {
                var a = getQueryVariable('utm_campaign');
                if(a){
                jQuery(this).val(a);
                }
                });
                jQuery('form').find('input.utm_term').each(function() {
                var a = getQueryVariable('utm_term');
                if(a){
                jQuery(this).val(a);
                }
                });
                jQuery('form').find('input.utm_content').each(function() {
                var a = getQueryVariable('utm_content');
                if(a){
                jQuery(this).val(a);
                }
                });
                });
                function createCookie(name,value,days) {    
                var expires = "";
                if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
                }
                document.cookie = name+"="+value+expires+"; path=/";
                }
                function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
                }
                function eraseCookie(name) {
                createCookie(name,"",-1);
                }
                var c_name = "_aaa_utmz";
                if(getQueryVariable("utm_source") != "") {
                createCookie("_aaa_utmz", getQueryVariable("utm_source") + "|" + getQueryVariable("utm_medium")+ "|" + getQueryVariable("utm_term")+ "|" + getQueryVariable("utm_campaign")+ "|" + getQueryVariable("utm_content"), 60);
                }
                else if (readCookie(c_name)){
                c_start=readCookie(c_name);
                var _pipe = c_start.split("|");
                jQuery("input[name=utm_source], .utm_source").val(_pipe[0]);
                jQuery("input[name=utm_medium], .utm_medium").val(_pipe[1]);
                jQuery("input[name=utm_term], .utm_term").val(_pipe[2]);
                jQuery("input[name=utm_campaign], .utm_campaign").val(_pipe[3]);
                jQuery("input[name=utm_content], .utm_content").val(_pipe[4]);
                }
                </script>

    <?php
}
add_action('wp_footer', 'wpb_hook_utm');


// function set_default_text_post( $content, $post ) {
// 	if ( '' !== $content or 'post' !== $post->post_type )
// 	{
// 		return $content;
// 	}
	
// 	return '[poenta_banner]';
// }
// add_filter( 'default_content', 'set_default_text_post', 10, 2 );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => __('הגדרות אתר'),
		'capability' => 'edit_posts',
		'menu_slug' => 'acf-site-settings',
	));
}

function my_scripts_and_styles(){

	$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/child.css'));
	wp_enqueue_style( 'child-css', get_stylesheet_directory_uri() . '/child.css', array(), $cache_buster, 'all' );
	$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/child.js'));
	wp_enqueue_script('child-js', get_stylesheet_directory_uri() . '/child.js', ['jquery'], $cache_buster, true);

	// Fix swipebox issue - disable swipebox
	global $avantgardia_js_data;
	wp_dequeue_script('swipebox-script');
	wp_dequeue_script('avantgardia-script');
	wp_enqueue_script( 'avantgardia-script-child', get_stylesheet_directory_uri() . '/js/avantgardia.js', array( 'jquery', 'packery-script', 'jquery-ui-autocomplete', 'jquery-ui-sortable' ), '20151101', true );
	wp_localize_script( 'avantgardia-script-child', 'avantgardia_data', $avantgardia_js_data );
	wp_localize_script( 'avantgardia-script-child', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . avantgardia__( 'expand child menu', 'newspeak' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . avantgardia__( 'collapse child menu', 'newspeak' ) . '</span>',
	) );

	wp_dequeue_style('avantgardia-fonts');
	$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/assets/css/fonts.css'));
	wp_enqueue_style( 'poenta-fonts', get_stylesheet_directory_uri() . '/assets/css/fonts.css', array(), $cache_buster, 'all' );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_and_styles', 20);



// function remove_breadcrumb_title( $link_output) {
// 	if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
// 		$link_output = '';
// 	}
//    	return $link_output;
// }
// add_filter('wpseo_breadcrumb_single_link', 'remove_breadcrumb_title' );

function shortcode_button_script()
{
    if(wp_script_is("quicktags"))
    {
		$shortcodes = get_field('pnt_shortcodes', 'option');
		$ui_shortcodes = [];
		foreach ($shortcodes as $shortcode) {
			$ui_shortcodes [] = ['id' => $shortcode['id'], 'name' => empty($shortcode['name']) ? "Shortcode: {$shortcode['id']}" : $shortcode['name']];
		}
        ?>
            <script type="text/javascript">
				window.addEventListener('DOMContentLoaded', () => {
					const shortcodes = <?= json_encode($ui_shortcodes) ?>;
					
					shortcodes.forEach(shortcode => {
						QTags.addButton( // add the button
						`poenta_shortcode ${shortcode.id}`, // id
						shortcode.name, // label
						() =>  {
							var selected_text = getSel(); // Gets the selected element
							QTags.insertContent(`${selected_text}[pnt_shortcode kind=${shortcode.id}]`)
						} // then the callback for this button
					);
					})
					//this function is used to retrieve the selected text from the text editor
					function getSel()
					{
						var txtarea = document.getElementById("content");
						var start = txtarea.selectionStart;
						var finish = txtarea.selectionEnd;
						return txtarea.value.substring(start, finish);
					}
	
					QTags.addButton( // add the button
						"poenta_embed_fix_shortcode", // id
						"Embed Fix", // label
						callback // then the callback for this button
					);
	
					function callback()
					{
						var selected_text = getSel(); // Gets the selected element
						QTags.insertContent("[embed_fix]"+selected_text+"[/embed_fix]"); // list shortcode we added above
					}
				});
            </script>
        <?php
    }
}
 
add_action("admin_print_footer_scripts", "shortcode_button_script");


// add_action( 'after_body_open_tag', 'add_json_schema');
// function add_json_schema() {
// 	require_once(get_stylesheet_directory().'/functions-schema.php');
// 	if (is_singular('post')) {
// 		post_schema();
// 	}
// }

// add_filter( 'wpseo_schema_webpage', 'change_article_schema_type' );
// function change_article_schema_type( $data ) {
//   if (is_singular('post')) { 
// 	  	$data['image'] = $data['primaryImageOfPage']; 
// 		unset($data['primaryImageOfPage']);
// 		unset($data['breadcrumb']);
// 		$data['@type'] = 'NewsArticle'; 
// 		$data['headline'] = $data['name']; 
// 		$data['publisher'] = array(
// 			"@type" => "Organization",
// 			"name" => "פואנטה",
// 			"sameAs" =>  get_home_url(),
// 			"logo" =>  array(
// 				"@type" =>  "ImageObject",
// 				"name" =>  "PoentaLogo",
// 				"width" =>  "229",
// 				"height" =>  "105",
// 				"url" =>  "https://www.poenta.co.il/wp-content/uploads/2018/03/logo.png"
// 			)
// 		);
// 		$data['mainEntityOfPage'] = array(
// 			"@type" => "WebPage",
// 			"@id" => $data['url'],
// 		);
// 		$data['author'] = array(
// 			"@type" => "Person",
// 			"name" => get_the_author(),
// 		);
//   }
//   return $data; 
// }

add_filter( 'yoast_seo_development_mode', '__return_true' );

add_filter( 'wpseo_schema_organization', 'change_organization_schema_type' );
function change_organization_schema_type( $data ) {
	if (!$data['url']) {
		$data['url'] = get_home_url(); 
	}
	return $data; 
}

// add_filter( 'wpseo_schema_needs_article', 'poenta_remove_article' );
// function poenta_remove_article( $data ) { 
// 	return !is_singular('post') && !is_front_page();
// }

add_action('wp_ajax_nopriv_generate_available_tags', 'take_action_function');
add_action('wp_ajax_generate_available_tags', 'take_action_function');
function take_action_function() {
	$tags = get_tags(array(
		'hide_empty' => false
	));

	$file = fopen(wp_upload_dir()['basedir']."/tags.csv","w");
	fputs($file, chr(0xFF) . chr(0xFE));
	foreach ($tags as $tag) {
		$fields = array($tag->term_id, $tag->name, '/tag/'.$tag->slug);
		$out = '';
		foreach ($fields as $k => $v){
			$fields[$k] = mb_convert_encoding($v, 'UTF-16LE', 'UTF-8');          
		}

		// UTF-16LE tab
		$out = implode(chr(0x09).chr(0x00), $fields);
		// UTF-16LE new line
		fputs($file, $out.chr(0x0A).chr(0x00));
	}
	fclose($file);
	wp_send_json( $tags );
	die;
}


function roi_debug($log, $force = false) {
	if (true || $force) {
		$logLine = date('d-m-y H:i:s').': '.$log.PHP_EOL;
		file_put_contents(WP_CONTENT_DIR.'/roi.log', $logLine, FILE_APPEND);
	}
}
add_action('wp_ajax_nopriv_csv_delete_tags', 'csv_delete_tags_function');
add_action('wp_ajax_csv_delete_tags', 'csv_delete_tags_function');
function csv_delete_tags_function() {
	$filePath = wp_upload_dir()['basedir']."/תגיות למחיקה 22.10.20.csv";
	$result = array();
	roi_debug('START!!. reading file: '.$filePath);
	if (($handle = fopen($filePath, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			for ($c=0; $c < sizeof($data); $c++) {
				$data[$c] = trim(iconv('CP1255', 'UTF-8', $data[$c]));
			}
			roi_debug('Processing tag: '.$data[0]. '('.$data[1].') -> '.$data[3]);
			
			$old_tag_url = $data[2];
			$old_tag_id = $data[0];
			$new_tag = get_term_by('slug', substr($data[3], 5), 'post_tag');
			roi_debug('Matching new tag: '.$new_tag->term_id. '('.$new_tag->name.')');
			$new_tag_id = $new_tag->term_id;
			$new_tag_url = get_tag_link($new_tag_id);
			
			roi_debug('Creating redirect from: '.$old_tag_url. ' to '.$new_tag_url);
			$redirectResults = Red_Item::create(array(
				'status' => 'enabled',
				"url" => $old_tag_url,
				"match_url" => $old_tag_url,
				'action_code' => 301,
				'action_type' => 'url',
				'action_data' => array('url' => get_tag_link($new_tag_id)),
				'group_id' => 1,
				'match_type' => 'url',
			));

			$posts = get_posts(array('tag_id' => $old_tag_id, 'posts_per_page' => -1));
			roi_debug('Number of posts in old tag: '.sizeof($posts));
			foreach ($posts as $curPost) {
				roi_debug('Adding tag: '.$new_tag_id.' to post: '.$curPost->ID);
				wp_set_post_tags($curPost->ID, array($new_tag_id), true);
			}
			roi_debug('Deleting tag: '.$old_tag_id);
			wp_delete_term($old_tag_id, 'post_tag' );
		  $result []= $data;
		}
		fclose($handle);
	} else {
		roi_debug('Failed to read file:'.$filePath);
	}
	wp_send_json( $redirectResults );
	die;
}

add_action('wp_ajax_nopriv_roi_test', 'roi_test_function');
add_action('wp_ajax_roi_test', 'roi_test_function');
function roi_test_function() {
	// global $wpdb;
	// $prefix = $wpdb->prefix;
	// $results = [];
	// $posts = array(21, 292, 738, 1753, 9864, 10155, 10222, 10383, 10441, 10463, 14021, 14022, 14031, 14884, 14902, 14903, 14953, 14955, 15102, 15104, 15105, 15106, 15319, 15323, 15326, 15327, 15546, 15561, 15621, 15626, 15627, 15645, 15646, 15647, 15691, 16503, 16505, 16506, 16939, 16943, 16944, 16945, 16946, 16948, 16949, 16950, 16951, 16996, 17010, 17022, 17397, 17399, 17400, 17403, 18275, 18276, 18287, 18332, 18338, 18339, 18340, 18342, 18392, 18603, 20159, 20167, 20168, 20179, 20180, 20265, 20543, 20984, 20997, 20998, 21113, 21114, 23035, 23036, 23039, 23651, 23665, 23668, 23691, 23696, 23697, 23703, 23869, 23873, 25164, 26109, 26120, 26159, 26160, 26161, 26162, 26194, 26206, 26240, 26243, 26245, 26246, 26248, 26251, 26253, 26254, 26256, 26257, 26258, 26265, 26266, 26267, 26268, 26269, 26270, 26271, 26272, 26273, 26274, 26275, 26320, 26329, 26330, 26336, 26338, 26339, 26340, 26341, 26343, 26373, 26374, 26425, 26427, 26432, 26485, 26505, 26507, 26509, 26510, 26511, 26513, 26516, 26517, 26519, 26520, 26521, 26522, 26523, 29485, 29817, 29818, 31585, 31586, 31587);
	// foreach ($posts as $pst) {
	// 	$result []= "https://www.poenta.co.il/wp-admin/post.php?post=$pst&action=edit";
	// }

	wp_mail('atlanteh@gmail.com', 'roi test', 'bla bla');
	wp_send_json( ['done' => 'yes'] );
	die;
}


// Remove p & br from wpfc7
add_filter( 'wpcf7_autop_or_not', '__return_false' );


add_filter( 'the_content', 'poenta_update_content' );
function add_author_box( $content )
{
	if (!is_singular( 'post' )) {
		return $content;
	}
	global $post;
	$excludedCats = get_field('author_exclude_cats', 'option');
	$currentCats = wp_get_post_categories($post->ID);
	$postExcludedCats = array_intersect($excludedCats, $currentCats);
	if (count($postExcludedCats)) {
		return $content;
	}

	$more_articles = get_field('more_articles');
	if (!empty($more_articles)) {
		$content .= '<div class="pnt-more-art">';
		$content .= '<div class="pnt-more-title">קראו עוד חדשות צרכנות באתר פואנטה</div>';
		
		$content .= '<ul>';
		foreach ($more_articles as $post_id) {
			$url = get_the_permalink($post_id);
			$title = get_the_title($post_id);
			$content .= "<li><a href='$url'>$title</a></li>";
		}
		$content .= '</ul>';	
		$content .= '</div>';	
	}

	if (get_field('show_facebook_banner')) {
		$content .= '<br />[poenta_banner]'.'<br/>';
	}

	$content .=
	'<div class="abx-wrapper">'.
	 '<div class="abx-img-wrap">'.
	   '<img src="'.get_field('author_img', 'option').'" class="abx-img" />'.
	 '</div>'.
	 '<div class="abx-txt-wrap">'.
	   '<a href="'.get_field('author_link', 'option').'" class="abx-name-link"><div class="abx-name">'.get_field('author_name', 'option').'</div></a>'.
	   '<div class="abx-desc">'.get_field('author_description', 'option').'</div>'.
	 '</div>'.
	 '<div class="abx-contact-wrap">'.
	   '<a href="mailto:dafna@poenta.co.il" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a>'.
	 '</div>'.
	'</div>';

	if (get_field('show_publish_banner')) {
		$publish_shortcode = get_field('pnt-share-shortcode', 'option') ?? '[poenta_publish]';
		$content .= '<br />'.$publish_shortcode.'<br/>';
	}

	if (get_field('show_ads')) {
		$ads = get_field('poenta_article_ads', 'option');
		$content .= '<br/>'.$ads.'<br/>';
	}

	$bottom_content = get_field('bottom_content');
	if (!empty($bottom_content)) {
		$content .= '<br/>'.$bottom_content;
	}

//   if (wp_is_mobile()) {
// 	  $content .= '<div class="OUTBRAIN" data-src="DROP_PERMALINK_HERE" data-widget-id="SB_1"></div> <script type="text/javascript" async="async" src="//widgets.outbrain.com/outbrain.js"></script>';
//   }
  return $content;
}

function poenta_wrap_tables($content) {
	// $pattern = '/(<table.*</table>)/';
	$replacement = '<div class="pnt-table-wrapper">$1</div>';
	$content = preg_replace( '/(<table.*?<\/table>)/s', $replacement, $content );
	return $content;
}
function poenta_update_content($content) {
	$content = add_author_box($content);
	$content = poenta_wrap_tables($content);

	return $content;
}

function is_site_admin(){
    return in_array('administrator',  wp_get_current_user()->roles);
}


function pnt_body_class($classes) {
    $classes[] = 'ag-no-loader';
    return $classes;
}

add_filter('body_class', 'pnt_body_class');

function poenta_change_og_image_for_archives($img_url) {
	$overrideImgId = null;
	if (is_post_type_archive(array('sales'))) {
		$overrideImgId = get_field('sales_archive_og_img', 'option');
	} else if (is_archive()) {
		$obj = get_queried_object();
		$overrideImgId = get_field('share_og_img', $obj);
	}

	if ( $overrideImgId ) {
		$og_image = wp_get_attachment_image_src( $overrideImgId, 'featured', false );
		return $og_image[0];
	}
	return $img_url;
}
add_filter( 'wpseo_opengraph_image', 'poenta_change_og_image_for_archives', 29 );
add_filter( 'use_block_editor_for_post', '__return_false' );
add_filter( 'use_widgets_block_editor', '__return_false' );

add_filter('wpseo_breadcrumb_single_link', 'remove_breadcrumb_title' );
function remove_breadcrumb_title( $link_output) {
	if (!is_singular(['post', 'sales'])) {
		return $link_output;
	}
	if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
		$link_output = '';
	}
   	return $link_output;
}

function pnt_order_acf_relationship_by_date($args) {
	$args['orderby'] = 'date'; 
	$args['order'] = 'desc'; 
	$args['post_status'] = 'publish'; 
	return $args;
}
add_filter( 'acf/fields/relationship/query', 'pnt_order_acf_relationship_by_date');


add_filter( 'comment_form_defaults', 'custom_reply_title' );
function custom_reply_title( $defaults ){
  $defaults['title_reply_before'] = '<div id="reply-title" class="comment-reply-title">';
  $defaults['title_reply_after'] = '</div>';
  return $defaults;
}
<?php

/**

 * The template for displaying all single posts and attachments.

 *

 * @package Avantgardia

 * @subpackage Newspeak

 * @since Newspeak 1.0

 */

 

global $avantgardia_global;

 

get_header(); 

?>


	<div id="primary" class="content-area">

		<main id="main" class="site-main">

		<?php 

			if ( have_posts() ) {

				

				the_post(); 

				

				$post_template = trim( get_post_meta( get_the_ID(), 'avantgardia_post_layout', true ) );

				

				if( empty( $post_template ) || 'default' == $post_template ) {					

					$post_template = avantgardia_get_option( 'avantgardia_singe_post_type', 'type-1' );

				}
								

				get_template_part( 'content/posts/single', $post_template );

				?>
				<div style="display:none">
				<?php print_r(get_field('remove_comments')); ?>
				</div>
				<?php

				if ( comments_open() || get_comments_number() ) {

					if(get_field('remove_comments')!=1){comments_template();}

				}

			} else {

				get_template_part( 'content', 'none' );

			}

		?>

		

		</main><!-- .site-main -->

	</div><!-- .content-area -->

	

<?php get_sidebar(); ?>

<?php get_footer(); ?>


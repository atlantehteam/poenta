<?php
/**
 * The sidebar containing the main widget area
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */

 $hide_for_singles = get_field('sidebar_hide_for_singles', 'option');

 if (is_page($hide_for_singles)) {
	 return;
 }

 global $avantgardia_global;
 
if( $avantgardia_global->has_sidebar ) : ?>

	<div id="sidebar" class="sidebar">
	 
<?php
	if ( ( is_active_sidebar( 'avantgardia-sidebar-1' ) && !$avantgardia_global->custom_sidebar_active  )  || ( avantgardia_is_woocommerce() && is_active_sidebar( 'avantgardia-sidebar-woocommerce' ) ) || $avantgardia_global->custom_sidebar_active ) : ?>
		<div id="secondary" class="sidebar-content secondary">

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav id="social-navigation" class="social-navigation" role="navigation">
					<?php
						// Social links navigation menu.
						wp_nav_menu( array(
							'theme_location' => 'social',
							'depth'          => 1,
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>',
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>
			<?php if( avantgardia_is_woocommerce() && is_active_sidebar( 'avantgardia-sidebar-woocommerce' ) ) { ?>
				<div id="widget-area" class="widget-area" role="complementary">
					<?php dynamic_sidebar( 'avantgardia-sidebar-woocommerce' ); ?>
				</div><!-- .widget-area -->
			<?php } else if ( is_active_sidebar( 'avantgardia-sidebar-1' ) || $avantgardia_global->custom_sidebar_active ) { ?>
				<div id="widget-area" class="widget-area" role="complementary">
					<?php 
					if( $avantgardia_global->custom_sidebar_active ) {
						dynamic_sidebar( $avantgardia_global->custom_sidebar ); 
					} else {
						dynamic_sidebar( 'avantgardia-sidebar-1' ); 
					}
					?>
				</div><!-- .widget-area -->
			<?php } ?>

		</div><!-- .sidebar-content-->

	<?php endif; ?>
	
	</div>
<?php endif; ?>

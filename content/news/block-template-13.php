<?php
/**
 * Template for displaying news block created with News Page Builder.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
 
global $avantgardia_news_posts;
$posts = $avantgardia_news_posts['posts'];
?>

<div class="ag-news-blocks-wrap" data-total-pages="<?php echo esc_attr( $avantgardia_news_posts['pages'] ); ?>">
	<div class="ag-row-wrap">
	<?php
	if( !empty( $posts[0] ) ) { 
		avantgardia_setup_postdata( $posts[0] );
		get_template_part( 'content/post-list/block', 'full-image-title-text' );
	}
	?>
	</div>
	<div class="ag-row-wrap">
	<?php
	for( $i = 1; $i < count( $posts ); $i++ ) {
		avantgardia_setup_postdata( $posts[ $i ] );
		get_template_part( 'content/post-list/block', 'small-image-title' );
	}
	?>
	</div>
</div>
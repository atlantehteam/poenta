<?php 

/**

 * Template for displaying single post content.

 *

 * @package Avantgardia

 * @subpackage Newspeak

 * @since Newspeak 1.0

 */

 

global $avantgardia_global; 

?>



<article id="post-<?php the_ID(); ?>" <?php post_class('type-1'); ?>>
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	}
	?>
	<div class="head-stripe">

		<?php 

		if( is_attachment() ) {

			$cat_link = '#';

			$cat_title = avantgardia__( 'Attachment', 'newspeak' );

		} else {

			$post_cat = avantgardia_get_post_cat(); 

			$cat_link = esc_url( get_category_link( $post_cat->term_id ) );

			$cat_title = esc_html( $post_cat->name );

		}

		?>

				<div class="cat-title cat-<?php echo $post_cat->term_id; ?>"><a class="post-category-link" href="<?php echo esc_url( get_category_link( $post_cat->term_id ) ); ?>"><?php echo esc_html( $post_cat->name ); ?></a></div>
	</div>

	<div class="single-post-content">
<?php         if($post_cat->term_id != 5717) { ?>
		<div class="single-post-meta">

			<time class="single-post-date" datetime="<?php the_time( 'c' ); ?>"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_time( 'G:i - l, d F Y' ); ?></time>

			<span class="vcard author author_name"><?php avantgardia_the_post_author(); ?></span>

			<div style="clear:both;"></div>

		</div>
<?php } ?>
		<header>

			<?php

			if( $avantgardia_global->is_singular ) {
				$title = get_field('post_single_title');
				if (!$title) {
					$title = get_the_title();
				}
				echo '<h1 class="entry-title">'.$title.'</h1>';
			} else {

				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 

			}

				?>

		</header>

		<div class="entry-content clearfix"><div class="pretext">
		<?php 
			if(has_excerpt()) {
				the_excerpt();
			}
		?>
		</div>
		
		<?php if($post_cat->term_id == 5717){ ?>
		<div class="single-post-meta">
			<time class="single-post-date" datetime="<?php the_time( 'c' ); ?>"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_time( 'G:i - l, d F Y' ); ?></time>
			<?php avantgardia_the_post_author(); ?>
		</div>
		<?php } ?>

		<?php if( $avantgardia_global->enable_shares || $avantgardia_global->show_view_count ) : ?>
		<div class="single-post-social">
			<?php
				if ( $avantgardia_global->enable_shares ) {
					if (! dynamic_sidebar('posts-social-buttons')):endif;
					//avantgardia_shares();
				}
			?>
		</div>
		<?php endif; ?>
		
		<?php 

			$image_height = get_post_meta( get_the_ID(), 'avantgardia_header_image_height', true );

			$classes = '';

			if( !empty( $image_height ) ) {

				$classes = get_post_meta( get_the_ID(), 'avantgardia_header_image_align', true );

			}
			if ($post_cat->term_id != 5717) {
				avantgardia_single_post_media( 'large', $classes, $image_height ); 
				$thumbnail_id    = get_post_thumbnail_id($post->ID);
				$thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

				if ($thumbnail_image && isset($thumbnail_image[0])) {
					echo '<span class="img-title">'.$thumbnail_image[0]->post_excerpt.'</span>';
				}
			}
		?>

		<?php
		the_content(); ?>
		</div>

		<?php avantgardia_single_post_meta(); ?>

		<?php avantgardia_get_post_navigation(); ?>

		<?php			

		//if( $avantgardia_global->is_single && avantgardia_has_related_posts() ) {

			//avantgardia_related_posts();

		//}

		if ($avantgardia_global->is_single) {

			$related_posts_number = intval( avantgardia_get_option( 'avantgardia_related_posts_number' ) );
			
			if( $related_posts_number > 0 ){
				
		        $tags = array();//wp_get_post_tags( $post->ID );
				$cats = wp_get_post_categories( $post->ID );
				$news_id = '5671';

				if (is_user_logged_in()) {
					if (($key = array_search($news_id, $cats)) !== false) {
					    unset($cats[$key]);
					}
				}

				//if (is_user_logged_in()) {echo '<pre>'; print_r($cats); echo '</pre>';}
				
		        if( count( $tags )  || count( $cats ) ) {
		            $tag_ids = array();
					foreach($tags as $tag) {
						$tag_ids[] = $tag->term_id;
					}
					$exclude_posts[] = $post->ID;
											
					$args = array(
						'post__not_in' => $exclude_posts,
						'posts_per_page' => $related_posts_number,
						'orderby' => 'rand',
    					'order'    => 'ASC',
						'date_query' => array(
							array(
								'after' => '1 month ago'
							)
						)
					);
					
					if( count( $tag_ids ) ) {
						$args['tag__in'] = $tag_ids;
					}
					
					if( count( $cats ) ) {
						$args['category__in'] = $cats;
					}

					$my_query = new WP_Query( $args );

					if (count($my_query->posts)) :
					?>

					<div class="section-title related-posts-section"><?php echo avantgardia__( 'Related news', 'newspeak' ); ?></div>
						<div class="related-posts-wrap 55555">

					<?php
					endif;
			 
					while( $my_query->have_posts() ) {
						$my_query->the_post();
						$related_posts[] = $post;
						
						get_template_part( 'content/post-list/block', 'third-image-title' );
					}
					
					wp_reset_query();

					if (count($my_query->posts)) :
					?>
			
						</div><!-- .related-posts-wrap -->

					<?php
					endif;
				}

				$latest_args = array(
					'post__not_in' => $exclude_posts,
					'posts_per_page' => $related_posts_number,
					'orderby' => 'date',
					'order'    => 'DESC',
				);
				$latest_posts = get_posts( $latest_args );
				if (count($latest_posts)) {
					?>
					<div class="section-title latest-posts-section"><?php echo __( 'מאמרים אחרונים', 'poenta' ); ?></div>
					<div class="latest-posts-wrap">
					<?php
					foreach ($latest_posts as $post) {
						setup_postdata( $post );
						get_template_part( 'content/post-list/block', 'third-image-title' );
					}
					wp_reset_postdata();
					?>
					</div><!-- .latest-posts-wrap -->
					<?php
				}
			}

		}

		?>

	</div>

</article>
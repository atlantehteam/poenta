<?php 

/**

 * This is basic template for displaying post content.

 *

 * @package Avantgardia

 * @subpackage Newspeak

 * @since Newspeak 1.0

 */

 

global $avantgardia_global; 

?>



<article id="post-<?php the_ID(); ?>" <?php post_class( 'default-post-list' ); ?>>

	<div class="head-stripe">

		<?php $post_cat = avantgardia_get_post_cat(); ?>

		<h1 class="cat-title <?php $post_cat->term_id; ?>"><a class="post-category-link" href="<?php echo esc_url( get_category_link( $post_cat->term_id ) ); ?>"><?php echo esc_html( $post_cat->name ); ?></a></h1>

	</div>
	<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	}
	?>
	<?php avantgardia_single_post_media(); ?>

	<div class="single-post-content">

		<div class="single-post-meta">

			<time class="single-post-date" datetime="<?php the_time( 'c' ); ?>"><a href="<?php the_permalink(); ?>"><?php the_time( avantgardia_get_date_format() ); ?></a></time>

			<?php if( $avantgardia_global->is_single ) { ?>

			<div class="single-post-author">

				<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">

					<span><?php printf( avantgardia__( 'Author: %s', 'newspeak' ), get_the_author() ); ?></span>

				</a>

			</div>

			<?php } ?>

		</div>

		<header>

			<?php

			if( $avantgardia_global->is_singular ) {

				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 

			} else {

				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 

			}

			?>

			

		</header>

		<?php if( $avantgardia_global->enable_shares || $avantgardia_global->show_view_count ) : ?>

		<div class="single-post-social">

			<?php if( $avantgardia_global->show_view_count ) { ?>

			<div class="single-post-view-count"><?php printf( avantgardia__( 'Views: %s', 'newspeak' ), avantgardia_get_view_count() ); ?></div>

			<?php } ?>

			<?php

				if ( $avantgardia_global->enable_shares ) {

					avantgardia_shares();

				}

			?>

		</div>

		<?php endif; ?>

		<div class="entry-content clearfix"><?php 

			if( $avantgardia_global->is_singular ) {

				the_content();

			} else {

				the_content( avantgardia__( 'Read more', 'newspeak' ) );

			}?></div>

		<?php avantgardia_single_post_meta(); ?>

		<?php

		if( $avantgardia_global->is_single ) {

			avantgardia_related_posts();

		}

		?>

	</div>

</article>
<?php
/**
 * Template for displaying news block created with News Page Builder.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
 ?>

<div class="ag-head-ticker">
	<?php
	$ticker_num_posts = intval( avantgardia_get_option( 'avantgardia_head_ticker_post_count' ) );
	$ticker_cat_id = intval( avantgardia_get_option( 'avantgardia_head_ticker_cat' ) );
	$ticker_cat = get_the_category_by_ID( $ticker_cat_id );
	if( is_wp_error( $ticker_cat ) ) {
		$ticker_cat = '';
	}
	
	$ticker_posts = get_posts( array( 'posts_per_page' => $ticker_num_posts/*, 'cat' => $ticker_cat_id*/ ) );
	?>
	<div class="ag-head-ticker-cat">חם מהתנור</div>
	<div class="ag-head-ticker-content">
		<div class="ag-head-ticker-sliding-panel">
			<?php foreach( $ticker_posts as $ticker_post ) { ?>
			<div class="item"><span class="ag-head-ticker-time"><?php echo mysql2date('d-m-Y',$ticker_post->post_date ); ?></span><span class="spacer-dash">-</span><a href="<?php echo esc_url( get_permalink( $ticker_post->ID ) ); ?>"><span class="ag-head-ticker-title"><?php echo esc_html( $ticker_post->post_title ); ?></span></a><div class="item-separator"><i class="fa fa-circle"></i></div></div>
			<?php } ?>
		</div>
	</div>
	<div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div></div>
</div>


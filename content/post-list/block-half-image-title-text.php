<?php
/**
 * Template for displaying news post.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
 
global $avantgardia_global; 
$post_id = get_the_ID();
?>
<div <?php post_class( 'post-list news-post-block block-half image title text' ); ?>>
	<?php if( has_post_thumbnail() ) { ?>
	<div class="news-list-media">
		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title() ?>">
			<?php echo avantgardia_get_post_image( $post_id, 'avantgardia-block-large-wide', 'news-list-main-image', true, get_the_title() ); ?>
			<time class="post-block-date" datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_time( avantgardia_get_date_format() ); ?></time>
				
			<div class="post-block-format"><?php echo avantgardia_get_post_format(); ?></div>
		</a>
	</div><!-- .post-list-media -->
	<?php } ?>
	
	<div class="news-list-content-wrapper">
		<header class="news-list-header">
			<?php
				the_title( sprintf( '<div class="entry-title"><a href="%s" rel="bookmark" title="%s">', esc_url( get_permalink() ), get_the_title() ), '</a></div>' );			
			?>
		</header><!-- .post-list-header -->
		<div class="news-list-excerpt"><?php the_excerpt(); ?></div>		
		<footer class="news-list-footer"><?php avantgardia_grid_entry_meta( $post_id ); ?></footer><!-- .post-list-footer -->
	</div>
</div><!-- #post-<?php the_ID(); ?> -->
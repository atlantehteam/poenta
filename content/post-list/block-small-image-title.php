<?php
/**
 * Template for displaying post in list.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
 
global $avantgardia_global; 
$post_id = get_the_ID();
?>
<article <?php post_class( 'post-list news-post-block block-small image title' ); ?>>
	<?php if( has_post_thumbnail() ) { ?>
	<div class="news-list-media">
		<a href="<?php the_permalink(); ?>" rel="bookmark">
			<?php echo avantgardia_get_post_image( $post_id, 'avantgardia-block-small-square', 'news-list-main-image', true, get_the_title() ); ?>		
		</a>
	</div><!-- .post-list-media -->
	<?php } ?>
	
	<div class="news-list-content-wrapper">
		<header class="news-list-header">
			<?php
				the_title( sprintf( '<div class="entry-title"><a href="%s" rel="bookmark" title="%s">', esc_url( get_permalink() ), get_the_title() ), '</a></div>' );
			?>
		</header><!-- .post-list-header -->	
		<footer class="news-list-footer"><?php avantgardia_grid_entry_meta( $post_id ); ?></footer><!-- .post-list-footer -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

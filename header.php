<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>


	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">	
	<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" />

	<?php pnt_header_scripts() ?>
	<?php avantgardia_post_meta_data(); ?>
	<?php wp_head(); ?>
	<!-- MailMunch for פואנטה -->
<!-- Paste this code right before the </head> tag on every page of your site. -->
<script src="//a.mailmunch.co/app/v1/site.js" id="mailmunch-script" data-mailmunch-site-id="416559" defer></script>

</head>
<?php global $avantgardia_global; ?>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DXMDC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php do_action('after_body_open_tag'); ?>

<?php /*
<div id="global-loader">
	<div class="sk-folding-cube">
		<div class="sk-cube1 sk-cube"></div>
		<div class="sk-cube2 sk-cube"></div>
		<div class="sk-cube4 sk-cube"></div>
		<div class="sk-cube3 sk-cube"></div>
	</div>
</div>
*/ ?>
<div id="page" class="hfeed site">
<?php $has_primary_menu = has_nav_menu( 'primary' ); ?>

<?php
if (false && amp_is_request() && wp_is_mobile()) {
?>
<!-- code from sekindo - Poenta_MobileBanner_360/360 -->
<amp-ad type="sekindo" data-spaceId="102648" width="360" height="360"></amp-ad>
<!-- code from sekindo -->
<?php
}
?>

<header id="masthead" class="site-header clearfix <?php if( !$has_primary_menu ) { echo 'no-primary-menu'; } ?>">
	<?php avantgardia_start_semantic_element(); ?>

	<div class="header-content-wrap clearfix">

			<div class="site-logo-wrapper clearfix">

				<div id="main-logo">

					<a href="<?php echo esc_url( home_url() ); ?>" rel="home">

					<?php 

					$main_logo = avantgardia_get_option( 'avantgardia_main_logo' );

					if( !empty( $main_logo ) ) {

					?>

						<img src="<?php echo esc_url( $main_logo ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="site-logo" />

					<?php 

					} else {

					?>

						<h2 class="main-site-title"><?php bloginfo( 'name' ); ?></h2>

					<?php

					}					

					?>					

					</a>

				</div>

				<div class="header-promo"><?php pnt_header_banner(); ?></div>

				<div class="top-search">
					<form method="get" action="<?php echo esc_url( home_url() ); ?>" class="top-search-form focus" id="top-search-form">

						<div class="search-button"><i class="fa fa-search"></i></div>
						<div class="search-input-wrap">
							<input type="text" name="s" class="s field" value="" autocomplete="off" />
							<input type="submit" value="Search" class="searchsubmit" name="submit" />
						</div>
					</form>

				</div>
			</div>

			<div class="site-branding">

				<div class="header-width-wrap">



					<?php if ( $has_primary_menu ) : ?>
					<div id="search-social">
										<div class="info-links">
						<?php avantgardia_social_links(); ?>
					</div>
										<div id="small-header-logo">

						<a href="<?php echo esc_url( home_url() ); ?>" rel="home">

							<?php

							$secondary_logo = avantgardia_get_option( 'avantgardia_secondary_logo' );

							if( !empty( $secondary_logo ) ) {

							?>

							<img src="<?php echo esc_url( avantgardia_get_option( 'avantgardia_secondary_logo' ) ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="site-logo" />

							<?php

							} else {

							?>

							<h2 class="secondary-site-title"><?php bloginfo( 'name' ); ?></h2>

							<?php

							}

							?>

						</a>

					</div>
						<div class="header-search">
						<form method="get" action="<?php echo esc_url( home_url() ); ?>" class="header-search-form" id="header-search-form">

							<div class="search-button"><i class="fa fa-search"></i></div>

							<div class="search-input-wrap">

								<input type="text" name="s" class="s field" value="" autocomplete="off" />

								<input type="submit" value="Search" class="searchsubmit" name="submit" />

							</div>

						</form>

				</div>
				</div>
						<nav id="site-navigation" class="main-navigation">

							<div class="ag-mobile-menu">

								<div class="ag-mobile-menu-button"><i class="fa fa-bars"></i></div>

							<?php

								// Mobile navigation menu 

								wp_nav_menu( array(

									'menu_class'     => 'ag-nav-menu-mobile',

									'container_class' => 'mobile-menu-container',

									'theme_location' => 'primary',

									'walker' => new avantgardia_walker_nav_menu()

								) ); 

							?>

							</div><!-- .ag-mobile-menu -->

							<?php

								

								// Primary navigation menu.

								wp_nav_menu( array(

									'menu_class' => 'ag-nav-menu-primary ag-header-menu clearfix',

									'container_class' => 'main-menu-container',

									'theme_location' => 'primary',

									'walker' => new avantgardia_walker_nav_menu(),

									'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s' . avantgardia_dropdown_nav_menu( 'primary' ) . '</ul>'

								) );

							?>
						</nav><!-- .main-navigation -->

					<?php endif; ?>



				</div>

			</div><!-- .site-branding -->

	</div>

	<?php avantgardia_end_semantic_element(); ?>

</header><!-- .site-header -->
	<?php get_template_part( 'inc/header', 'image' ); ?>
	
	<div id="content" class="site-content clearfix">
		<?php if( $avantgardia_global->show_head_ticker ) { get_template_part( 'content/header/header', 'ticker' ); } ?>
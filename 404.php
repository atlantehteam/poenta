<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content nf" role="main">

			<header class="page-header">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/paper.jpg' ?>"/>
				<h1 class="page-title red"><?php _e( '404', 'newspeak' ); ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<h2><?php _e( '<span class="red">אופס!</span> לא הצלחנו למצוא את הדף שחיפשת!', 'newspeak' ); ?></h2>
					<p><?php _e( 'אבל יש לנו כתבות מעניינות אחרות. רוצים להציץ?', 'newspeak' ); ?></p>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->
		</div><!-- #content -->
	</div><!-- #primary -->
	<div class="content-area nf-recents">
		<?php
			$posts = wp_get_recent_posts(array(
				'numberposts' => 5, // Number of recent posts thumbnails to display
				'post_status' => 'publish' // Show only the published posts
			), OBJECT_K );
		?>
		<div class="ag-news-blocks-wrap">
			<div class="ag-row-wrap">
			<?php
			if( !empty( $posts[0] ) ) { 
				avantgardia_setup_postdata( $posts[0] );
				get_template_part( 'content/post-list/block', 'full-image-title-text' );
			}
			?>
			</div>
			<div class="ag-row-wrap">
			<?php
			for( $i = 1; $i < count( $posts ); $i++ ) {
				avantgardia_setup_postdata( $posts[ $i ] );
				get_template_part( 'content/post-list/block', 'small-image-title' );
			}
			?>
			</div>
		</div><!-- ag-news-blocks-wrap -->
		<a class="ag-fsubmit" href="<?php echo get_home_url(); ?>">חזרה לדף הבית</a>
	</div><!-- content-area -->
<?php get_footer(); ?>
<?php
/**
 * The main template file for archives.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
global $avantgardia_global; 
 
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main clearfix">

		<?php if ( have_posts() ) : ?>
			
			<div class="archive-header">
				<div class="archive-type"><?php the_archive_title( '<h1 class="cat-title">', '</h1>' ); ?></div>
			</div>
			<?php
			$archive_description = category_description();
			if (!empty($archive_description)) {
				echo "<div class='archive-description'>$archive_description</div>";
			}
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
			
			<?php
			
			$template_type = avantgardia_get_archive_template_name();
						
			/*
			 * We need to check if displaying of grid on front page is checked. If it is, 
			 * template files for displaying post block will automatically be loaded using 
			 * avantgardia_get_grid_element() function.
			 */
			if( $avantgardia_global->grid_on_archive ) {
				avantgardia_start_grid();
			} else { 
				?><div class="archive-posts-wrap post-content-wrapper"><?php 
			}
			$i = 0;			
			// Start the loop.
			while ( have_posts() ) : the_post();

				if( $avantgardia_global->grid_on_archive ) {
					/*
					* Load and display grid template element.
					*/
					avantgardia_get_grid_element();
				} else {
					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content/post-list/block', $template_type );
					$i++;
					if($i%3==0)
						echo do_shortcode('[do_widget id=custom_html-4 class=newsfeed-ad title=false]');
				}

			// End the loop.
			endwhile;
						
			if( $avantgardia_global->grid_on_archive ) {			
				avantgardia_end_grid();
			} else {
				
				// Previous/next page navigation.
				avantgardia_get_page_navigation( 'archive-load-more' );
								
				?></div><!-- .archive-posts-wrap --><?php 
			}

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>

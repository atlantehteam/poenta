<?php
 if ( !function_exists( 'avantgardia_get_post_cat' ) ) :
	/**
	 * Gets post category (only on if there are many) or requested post.
	 * If no post_id is set, global post ID will be used.
	 *
	 * @since Newspeak 1.0
	 */
	 function ja_remove_hentry( $class ) {
		$class = array_diff( $class, array( 'hentry' ) );	
		return $class;
	}
	add_filter( 'post_class', 'ja_remove_hentry' );

	function avantgardia_get_post_cat( $post_id = '' ) {

		$post_id = avantgardia_check_post_id( $post_id );
		
		$category_ids = wp_get_post_categories( $post_id );
		
		$category = '';
		
		if( !empty( $category_ids ) ) {
			$category = get_category( $category_ids[0] );
		} else {
			$category = new STDClass;
			$category->term_id = 0;
			$category->name = '';
		}
		$primary_cat_id=get_post_meta($post_id,'_yoast_wpseo_primary_category',true);
		if($primary_cat_id){
		$product_cat = get_term($primary_cat_id);
		//print_r($product_cat);
		$category =$product_cat;
		}
		return $category;
	}	
endif; // avantgardia_get_post_cat
if ( !function_exists( 'avantgardia_grid_entry_meta' ) ) :
/**
 * Generates post meta for the current post.
 *
 * @since Newspeak 1.0
 */	 
function avantgardia_grid_entry_meta( $post_id = '' ) {
    
    global $avantgardia_global, $post;
    
    $post_id = avantgardia_check_post_id( $post_id );
    
    $categories = wp_get_post_categories( $post_id );
    
    if( !empty( $categories ) ) {
        $cat = get_category( $categories[0] );
    } 
    $primary_cat_id=get_post_meta($post_id,'_yoast_wpseo_primary_category',true);
    if($primary_cat_id){
    $product_cat = get_term($primary_cat_id);
    //print_r($product_cat);
    $cat =$product_cat;
}
    $comment_count = get_comments_number();
            
    ?>
    <div class="grid-entry-meta">
                        
        <span class="meta-buttons-wrap">
        
            <span class="meta-data-icons">
            
            
                                
            <?php if( !empty( $cat ) ) : ?>
                <span class="post-category cat-<?php echo $cat->term_id;?>"><a href="<?php echo esc_url( get_category_link( $cat->term_id ) ); ?>"><?php  echo esc_html( $cat->name ); ?></a></span>
            <?php endif; ?>
            <time><i class="fa fa-clock-o" aria-hidden="true"></i><span><?php the_time( 'G:i' );echo " - "; echo get_the_date( 'F j, Y' ); ?></span></time>
            <?php edit_post_link( avantgardia__( 'Edit', 'newspeak' ), '<span class="edit-link">', '</span>' ); ?>	
            </span>
        </span>
    </div>
    <?php
}

endif; 

// avantgardia_grid_entry_meta
if ( !function_exists( 'avantgardia_shares' ) ) :
	/**
	 * Displays share icons if they are enabled.
	 *
	 * @since Newspeak 1.0
	 */
	function avantgardia_shares() {
		
		global $avantgardia_global, $post;
		
		if( $avantgardia_global->enable_shares ) { 
		
			$title = get_the_title();
			$excerpt = substr( strip_tags( $post->post_content ), 0, 140 ) . '...' ;
			$url = urldecode( get_the_permalink() );
			
			$image_id = get_post_thumbnail_id( $post->ID );		
			$thumb = avantgardia_get_image_src( $image_id, 'full' );
			$image_src = empty( $thumb[0] ) ? '' : $thumb[0];
		?>				
			<div class="single-post-share-icons post-share-data"  data-title="<?php echo esc_attr( $title ); ?>" data-url="<?php echo esc_url( $url ); ?>" data-description="<?php echo esc_attr( $excerpt ); ?>" data-thumbnail="<?php echo esc_url( $image_src ); ?>" >
				<div class="ag-ss-icon sffb-share"><span class="sfsh sffb"></span></div>
				<div class="ag-ss-icon sftw-share"><span class="sfsh sftw"></span></div>
				<div class="ag-ss-icon sfgp-share"><span class="sfsh sfgp"></span></div>
				<div class="ag-ss-icon sfln-share"><span class="sfsh sfln"></span></div>
				<div class="ag-ss-icon sfpt-share"><span class="sfsh sfpt"></span></div>
								<a  class="ag-ss-icon sfwa-share" href="https://api.whatsapp.com/send?text=<?php echo  esc_url( $url ) ; ?>"><i class="fa fa-whatsapp"></i></a>
					 <!-- Print -->
    <a class="ag-ss-icon sfpr-share" href="javascript:;" onclick="window.print()">
         <i class="fa fa-print"></i>
    </a>
<!-- Email -->
    <a class="ag-ss-icon sfem-share" href="mailto:daf.kfir@gmail.com?Subject=<?php echo $title . " - פואנטה"; ?>&amp;Body=<?php echo $excerpt . " " . $url ; ?>">
        <i class="fa fa-envelope"></i>
    </a>
			</div>
		<?php
		}
	}
	
endif; // avantgardia_shares

if ( !function_exists( 'avantgardia_the_post_author' ) ) :

	/**

	 * Prints post author html.

	 *

	 *

	 * @since Newspeak 1.0.1

	 */
function avantgardia_the_post_author() {

		

		global $avantgardia_global;

		

		if( $avantgardia_global->is_single && !$avantgardia_global->hide_author_on_single ) { 

		?>

			<div class="single-post-author">
				<span> - </span>
				<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">

					<span><?php printf( avantgardia__( ' %s', 'newspeak' ), get_the_author() ); ?></span>

				</a>

			</div>

		<?php 

		}		

	}

	

endif; // avantgardia_the_post_author
if ( !function_exists( 'avantgardia_comment_form' ) ) :

	/**

	 * Sets up options and displays comment form.

	 *

	 * @since Newspeak 1.0

	 */

	function avantgardia_comment_form() {

		

		global $user_identity;

		

		$commenter = wp_get_current_commenter();

		$req = get_option( 'require_name_email' );

		$aria_req = ( $req ? " aria-required='true'" : '' );

		$required_text = '*';

		

		$fields =  array(

		  'author' =>

			'<p class="comment-form-author comment-field">' .

			'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .

			'" size="30"' . $aria_req . ' placeholder="' . avantgardia__( 'Name', 'newspeak' ) . ( $req ? $required_text : '' ) . '"/></p>',



		  'email' =>

			'<p class="comment-form-email comment-field">' .			

			'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .

			'" size="30"' . $aria_req . ' placeholder="' . avantgardia__( 'Email', 'newspeak' ) . ( $req ? $required_text : '' ) . '" /></p>',

		);

		

		$args = array(

			'id_form'           => 'commentform',

			'id_submit'         => 'submit',

			'class_submit'      => 'submit',

			'name_submit'       => 'submit',

			'title_reply'       => '',

			'title_reply_to'    => avantgardia__( 'Leave a Reply to %s', 'newspeak' ),

			'cancel_reply_link' => avantgardia__( 'Cancel Reply', 'newspeak' ),

			'label_submit'      => avantgardia__( 'Post Comment', 'newspeak' ),

			'format'            => 'xhtml',

			'submit_button' => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',

			'submit_field' => '<p class="form-submit">%1$s %2$s</p>',

			'fields' => apply_filters( 'comment_form_default_fields', $fields ),

			'comment_field' =>  '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45"' . 

			' rows="8" aria-required="true" placeholder="' . _x( 'Comment', 'noun', 'newspeak' ) .'"></textarea></p>',



			'must_log_in' => '<p class="must-log-in">' .

				sprintf(

				  avantgardia__( 'You must be <a href="%s">logged in</a> to post a comment.', 'newspeak' ),

				  wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )

				) . '</p>',



			'logged_in_as' => '<p class="logged-in-as">' .

				sprintf(

				avantgardia__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'newspeak' ),

				  admin_url( 'profile.php' ),

				  $user_identity,

				  wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) )

				) . '</p>',



			'comment_notes_before' => '<p class="comment-notes"></p>',



			'comment_notes_after' => '<p class="after-form-notes"></p>',

		);

		

		comment_form( $args );

	}



endif; // avantgardia_comment_form



if ( ! function_exists( 'avantgardia_item_scope_meta' ) ) :
	function avantgardia_item_scope_meta() {
        return '';
	}
endif; // avantgardia_item_scope_meta
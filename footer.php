<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package Avantgardia
 * @subpackage Newspeak
 * @since Newspeak 1.0
 */
?>
		<?php get_sidebar(); ?>
		<footer id="colophon" class="site-footer clearfix">
		<div class="site-content">
			<?php avantgardia_start_semantic_element();  ?>
			<div class="footer-widgets  clearfix">
					<div class="footer-widgets-content ag-footer-widgets-count-4">
						<?php dynamic_sidebar( 'article-foot-sidebar' ); ?>
					</div>
			</div>

			<div class="site-info">
				<div class="site-info-content">
					<?php 
					$footer_logo = trim( avantgardia_get_option( 'avantgardia_secondary_logo' ) );
					if( !empty( $footer_logo ) ) {
					?>
					<?php }	
					$footer_text = trim( avantgardia_get_option( 'avantgardia_footer_text' ) );
					if( !empty( $footer_text ) ) {
					?>
					<?php } ?>
				</div>
			</div>
		<?php avantgardia_end_semantic_element(); ?>
		</div>
		<div class="footer-description-text"><div class="site-content"><?php echo wp_kses_post( $footer_text ); ?>
			<div class="sv" style="direction:ltr;">
		    <?php /* ?><p>Powered By Spring-Valley <a style="display:inline-block;vertical-align:middle;" href="https://www.spring-valley.co.il/"><img class="sv-logo" style="max-height:2em;" src="https://www.poenta.co.il/wp-content/themes/newspeak-child/images/logo.png" alt="Spring Valley Logo"></a></p><?php */ ?>
		</div>
		</div></div>
		</footer><!-- .site-footer -->
	</div><!-- .site-content -->
	
</div><!-- .site -->
<div id="resize-flags">
	<div id="max-width-960"></div>
	<div id="max-width-640"></div>
</div>
<div id="back-to-top" class="animated"><i class="fa fa-angle-up"></i></div>

<?php wp_footer(); ?>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        // Show all sections popup (when hidden in additional menu)
        jQuery('.menu-item-2058').addClass('popmake-2070');
        jQuery('.menu-item-2058 a').addClass('pum-trigger');

    }, false);


setTimeout(sayHi, 5000);
    
    function sayHi() {

		jQuery('input[name=utmfield]').val("<?php echo $_GET["utm_source"] ?? '' ?>");
		jQuery('input[name=utm_medium]').val("<?php echo $_GET["utm_medium"] ?? '' ?>");
		jQuery('input[name=utm_campaign]').val("<?php echo $_GET["utm_campaign"] ?? '' ?>");
		jQuery('input[name=utm_term]').val("<?php echo $_GET["utm_term"] ?? '' ?>");
		jQuery('input[name=utm_content]').val("<?php echo $_GET["utm_content"] ?? '' ?>");
	} 
</script>

<!-- Enable mobile menu click before interaction -->
<script>
window.addEventListener("DOMContentLoaded", function(){
	var $ = jQuery;
	$('.ag-mobile-menu-button').one('click.finishtogglemenu touchstart', function(){
		var $mobileMenu = $('.ag-mobile-menu')
			.addClass('open')
			.find('.ag-nav-menu-mobile')
			.slideDown(500);
			
			$('body').addClass('menu-open');
			$(this).off("click.finishtogglemenu")
	return false;
	});
});
</script>
<?php
pnt_footer_scripts();
?>
</body>
</html>

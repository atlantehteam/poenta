<?php

function main_article($post) { ?>
    <a class="tn-new main" href=<?= get_permalink($post) ?>>
        <div class="tn-new-img">
            <?= get_the_post_thumbnail($post, 'avantgardia-block-extra-large-wide') ?>
        </div>
        <div class="tn-new-txt">
            <h2 class="tn-new-title">
                <?= get_the_title($post) ?>
            </h2>
            <h3 class="tn-new-excerpt">
                <?= get_the_excerpt($post) ?>
            </h3>
        </div>
    </a>
<?php
}

function secondary_article($post) {
    if (!$post) {return;}
?>
    <a class="tn-new" href=<?= get_permalink($post) ?>>
        <div class="tn-new-img">
            <?= get_the_post_thumbnail($post, 'avantgardia-block-large-wide') ?>
        </div>
        <div class="tn-new-txt">
            <h2 class="tn-new-title">
                <?= get_the_title($post) ?>
            </h2>
        </div>
    </a>
<?php
}

    $categories = get_field('main_slider_category', 'option');
    $comma_separated_cat = implode (",", $categories);

    $posts = wp_get_recent_posts(array(
        'numberposts' => 5, // Number of recent posts thumbnails to display
        'post_status' => 'publish', // Show only the published posts
        'category'=> $comma_separated_cat,
    ), OBJECT_K );
    $main_post = array_shift($posts);
?>

<div class="top-news">
    <div class="top-news-container">
        <?php main_article($main_post) ?>
    </div>
    <div class="top-news-container">
        <?php
        for ($i=0; $i < sizeof($posts); $i+=2) { ?>
            <div class="top-news-row">
                <?php secondary_article($posts[$i] ?? null) ?>
                <?php secondary_article($posts[$i + 1] ?? null) ?>
            </div>
        <?php
        }
        ?>
    </div>
</div>
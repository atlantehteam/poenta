<?php



/* 

 * Template name: News Page Builder

 *

 * This template enables you to display news from diferent categories in the way and 

 * order that you want and that you defined trough News Page Builder.

 *

 * @package Avantgardia

 * @subpackage Newspeak

 * @since Newspeak 1.0

 */



global $avantgardia_global, $post;



the_post(); 	


get_header();

	get_template_part( 'page-templates/top-articles-widget' );
?>
	<div id="primary" class="content-area">		

		<main id="main" class="site-main">

			<?php if( !empty( $post->post_content ) ) { ?>

			<div class="entry-content clearfix">

			<?php do_action( 'avantgardia_after_entry_content_open' ); ?>

				<div class="single-post-content">

					<?php the_content(); ?>

				</div>				

			<?php do_action( 'avantgardia_after_entry_content_close' ); ?>

			</div><!-- .entry-content -->

			<?php } ?>

			<div class="news-page-builder-wrap">

				<?php  

				if( function_exists( 'avantgardia_print_news_blocks' ) ) {

					avantgardia_print_news_blocks();

				}

				?>

			</div>			

		</main><!-- .site-main -->

	</div><!-- .content-area -->
	<script>
jQuery(document).ready(function(){
   // console.log(jQuery('.wpp-category a:first-child').text());
    jQuery('.wpp-category a:not(:first-child)').remove();
   // jQuery('.wpp-category').remove(',');
   // jQuery('.wpp-category').detach().insertBefore('.wpp-date');
    //jQuery('.wpp-category a').not(':first').remove();
});
</script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
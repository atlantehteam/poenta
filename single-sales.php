<?php
    get_header(); 
    get_template_part( 'content/posts/single', $post_template );
    $image = get_the_post_thumbnail_url($post->ID, 'large' );
    $thumb = get_post_thumbnail_id($post->ID);
    $title = get_the_title();
    ?>
    <div class="product_area">
        <div class="product_section">
                <div class="product_gallery">
                        <img src="<?php echo $image; ?>" alt="">
                        <span class="img-title"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></span>
                </div>

                <div class="product_info">
                    <h3 class="product_title"><?= $title; ?></h3>
                    <div class="description entry-content"><?=the_content(); ?></div>
                    <?php if( get_field('cupon') ): ?>
                        <div class="cupon">קוד קופון: <?=the_field('cupon') ?></div>
                    <?php endif; ?>
                    <a href="<?=the_field('link') ?>" class="product_link" data-deal-name="<?=esc_attr($title) ?>" target="_blank">
                        <button class="product_button"><?=the_field('text') ?></button>
                    </a>
                    <div class="price"><?=the_field('price') ?></div>
                    
                </div>

        </div>
        <?php get_sidebar(); ?> 
    </div>
        

       
<?php get_footer(); ?>
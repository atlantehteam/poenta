<?php
function schema_start() {
    echo '<script type="application/ld+json">'
        .'{'
        .'  "@context": "http://schema.org",'
        .'  "inLanguage":"he",'
        .'  "publisher": {'
        .'    "@type":"Organization",'
        .'    "name":"פואנטה",'
        .'    "sameAs": "'.get_home_url().'",'
        .'    "logo": {'
        .'      "@type": "ImageObject",'
        .'      "name": "PoentaLogo",'
        .'      "width": "229",'
        .'      "height": "105",'
        .'      "url": "https://www.poenta.co.il/wp-content/uploads/2018/03/logo.png"'
        .'    }'
        .'},'
        ;
}
function schema_end() {
    echo '</script>';
}
function schema_type($type) {
    echo '  "@type": "'.$type.'",';
}
function schema_author() {
    $author = get_the_author_meta('display_name');
    if (!$author) {return;}
    echo '  "author": [{'
        .'    @type":"Person",'
        .'    "name": "'.get_the_author_meta('display_name').'"'
        .'  }],';
}
function schema_titles() {
    $title = get_the_title();
    $description = get_the_excerpt();
    if ($title) {
        echo '"headline": "'$title.'",';
    }
    if ($description) {
        echo '"description": "'$description.'",';
    }
    
    "description": "<?= get_the_excerpt() ?>",
}

function schema_image() {
    $attachmentId = get_post_thumbnail_id();
    if (!$attachmentId) {return;}

    $data = wp_get_attachment_image_src($attachmentId);
    echo
    '  "image": {'
        .'    "@type": "imageObject",'
        .'    "url": "'.$data[0].'",'
        .'    "height": "'.$data[1].'",'
        .'    "width": "'.$data[2].'"'
   .'  },'
    ;
}

function schema_dates() {
    $format = 'Y-m-d\TH:i:s';
    $modifiedDate = get_the_modified_date($format);
    $modifiedDate = get_the_date($format);

    echo
      '  "datePublished": "14/04/2020 13:59",'
     .'  "dateModified": "14/04/2020 15:58",'
    ;
}

function post_schema() {
    schema_start();
    schema_type('NewsArticle');
    schema_author();
    schema_titles();
    schema_image();
    schema_dates();
    schema_end();
?>

 

<?php
}
?>
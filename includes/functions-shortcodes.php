<?php
function get_top_posts_by_cat(){
    $id = get_the_ID();
	$ret="<ul class='acf-rpw-ul'>";
	$cats = avantgardia_get_post_cat($id);
    $args = array(
        'post_type' => 'post',
        'post__not_in' => array( $id ),
        'posts_per_page' => 3,
        'cat'     => $cats->term_id,
		'meta_key' => 'avantgardia_post_views',
    'orderby' => 'meta_value_num',
  
    );
    $query = new WP_Query($args); 
//var_dump($query);
if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post(); 
$cat = get_the_category();
$thumb_id = get_post_thumbnail_id(); // Get the featured image id.
			$img_url = wp_get_attachment_url( $thumb_id ); // Get img URL.
			// crop the image with the resizer class
			$image = acf_rpwe_resize( $img_url, 48, 48, true );
$ret .= '<li class="acf-rpw-li acf-rpw-clearfix"><article class="post-list list-medium post type-post status-publish format-standard has-post-thumbnail hentry"><div class="post-list-media">
<a class="acf-rpw-img" rel="bookmark" href="'. get_permalink() .'">
							<image src="' . $image. '" class="acf-rpw-left acf-rpw-thumb" />
						</a>
		</div>
			<div class="post-list-content-wrapper">
	<header class="post-list-header">
	<h3 class="acf-rpw-title"><a href="'. get_permalink() .'" rel="bookmark">'. get_the_title().'</a></h3>
	</header>
			<footer class="post-list-footer top-posts"><a class="post-category cat-'.$cat[0]->term_id.'" href="'.get_category_link($cat[0]->term_id).'">'.get_cat_name($cat[0]->term_id).'</a>
		<time><i class="fa fa-clock-o" aria-hidden="true"></i><span> ' .  get_the_time( 'G:i' ). " - " . get_the_date( 'F j, Y' ) . '</span></time></footer>
			</div>
	</article></li>';
 endwhile; endif; wp_reset_postdata(); 
 $ret.="</ul>";
 return $ret;
}
add_shortcode('top_posts','get_top_posts_by_cat');



// deals shortcode 
function deals($atts) {
	ob_start();
		?>
			<div class="products_area w-100">
				<?php
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$sales = new WP_Query( array( 
						'post_type' => 'sales', 
						'posts_per_page' => 6,
						'orderby' => 'date',
						'paged' => $paged,
						'order' => 'DESC'
					));
				?>
				<?php
				echo '<div class="deals-title"><a target="_blank" href="https://www.poenta.co.il/sales"><span>דילים מומלצים</span></a></div>';
				if( $sales->have_posts() ){
					while( $sales->have_posts() ){ $sales->the_post(); 
						$image = get_the_post_thumbnail_url($post->ID); 
					?>
						<a href="<?=the_permalink(); ?>" target="_blank" class="product">
							<div class="inner">
								<div class="image" ><img src="<?php echo $image; ?>" alt=""></div>
								<div class="title"><?php the_title(); ?></div>
								<div class="info">
									<div class="price"><?=the_field('price'); ?></div>
									<div><button>מעבר לרכישה</button></div>
								</div>
							</div>
						</a>
					<?php }
				} else { ?>
					לא נמצאו המלצות במערכת, נראה שיש בעיה.
				<?php }
				wp_reset_postdata();
			?>
		<?php
	$output = ob_get_contents();   
	ob_end_clean();   
	return $output;
}
add_shortcode( 'deals', 'deals' );






function poenta_banner_shortcode($atts) {
	ob_start();
	?>
		<div class="pnt-banner">
			<div class="pnt-banner-title">היה לכם מעניין?<br />
			לחצו <a href="https://www.facebook.com/poenta.il" target="_blank" rel="follow noopener"><span style="font-size: 14pt;">כאן </span></a> ופרגנו לנו בלייק. זה קל... 
			</div>
		<iframe
			src="https://www.facebook.com/plugins/page.php?locale=he_IL&href=https%3A%2F%2Fwww.facebook.com%2Fpoenta.il&tabs&width=500&height=250&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
			width="500"
			frameborder="0"
			allowTransparency="true" allow="encrypted-media"></iframe>
		</div>
	<?php
	$output = ob_get_contents();   
	ob_end_clean();   
	return $output;
}
add_shortcode( 'poenta_banner', 'poenta_banner_shortcode' );


function embed_fix_shortcode($atts, $content) {
	global $wp_embed;
	$embed_atts = '';
	if ($atts) {
		foreach ($atts as $key => $value) {
			$embed_atts .= $key.'='.$value.' ';
		}
	}
	$embed_code = $wp_embed->run_shortcode('[embed '.$embed_atts.']'.$content.'[/embed]');
	preg_match('/src="(.*)"/', $embed_code, $matches);
	$embed_code = preg_replace('/(src=").*(")/', '$1about:none$2', $embed_code);
	$output = "<div class='embed-wrapper' data-src='".$matches[1]."'>$embed_code</div>";
	return $output;
}
add_shortcode( 'embed_fix', 'embed_fix_shortcode' );


function poenta_publish_shortcode($atts) {
	ob_start();
	?>
		<div class="pnt-share">
			<div class="pnt-title"><?= get_field('pnt-share-title', 'option') ?> </div>
			<div class="pnt-subtitle"><?= get_field('pnt-share-subtitle', 'option') ?> </div>
			<?= do_shortcode( '[contact-form-7 id="'.get_field('cf7-form', 'option').'"]' ); ?>
		</div>
	<?php
	$output = ob_get_contents();   
	ob_end_clean();   
	return $output;
}
add_shortcode( 'poenta_publish', 'poenta_publish_shortcode' );


function poenta_hot_articles_shortcode($atts) {
	ob_start();
	$articles = get_field('hot_articles', 'option');
	echo '<ul class="pnt-articles">';
	foreach ($articles as $article) {
		?>
		<li class="pnt-article">
			<a href="<?= get_permalink($article) ?>">
				<?= get_the_post_thumbnail($article) ?>
				<div class="pnt-title"><?= $article->post_title ?> </div>
			</a>
		</li>
		<?php
	}
	echo '</ul>';
	$output = ob_get_contents();   
	ob_end_clean();   
	return $output;
}
add_shortcode( 'poenta_hot_articles', 'poenta_hot_articles_shortcode' );


function poenta_affiliate_list_impl($atts) {
    $id = $atts['id'] ?? '';
    if (!$id) {return;}
    
    $affiliateGroups = get_field('affiliates');
    if (empty($affiliateGroups)) {return;}

    $affiliate_items = null;
    foreach($affiliateGroups as $affiliate) {
        if ($affiliate['id'] == $id) {
            $affiliate_items = $affiliate['list'];
            break;
        }
    }
    
    if (empty($affiliate_items)) {
        return;
    }
    $rows = '';
    foreach ($affiliate_items as $item) {
        $rows .=
            '<tr class="pnt-aff-item">'.
                '<td>'.$item['store_name'].'</td>'.'<td>'.$item['price'].'</td>'.
                '<td class="pnt-aff-link"><a href="'.$item['store_link'].'" target="_blank">'.$item['cta_text'].'</a></td>'.
            '</tr>';
    }
    echo "<table class='pnt-affiliates'>$rows</table>";
}

function poenta_affiliate_list($atts) {
    ob_start();
    poenta_affiliate_list_impl($atts);
    $output = ob_get_contents();   
	ob_end_clean();   
	return $output;
}
add_shortcode( 'pnt_affiliate_list', 'poenta_affiliate_list' );

function pnt_custom_shortcodes($atts) {
    ob_start();

	$kind = $atts['kind'] ?? '';
    $shortcodes = get_field('pnt_shortcodes', 'option');
	$found_shortcode = array_column($shortcodes, null, 'id')[$kind] ?? false;
	if ($found_shortcode) {
		$device = wp_is_mobile() ? 'mobile' : 'desktop';
		
		if (in_array($device, $found_shortcode['show']) && isset($found_shortcode['content'])) {
			echo $found_shortcode['content'];
		}
	}
	return ob_get_clean();
}
add_shortcode( 'pnt_shortcode', 'pnt_custom_shortcodes' );

function pnt_frontpage_section_ad($atts) {
    ob_start();
?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- מודעת דף הבית לאתר החדש למעלה -->
<ins class="adsbygoogle"
     style="display:inline-block;height:90px;width: 100%;"
     data-ad-client="ca-pub-3724182673190727"
     data-ad-slot="1416244750"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
	return ob_get_clean();
}

/*function pnt_frontpage_section_ad($atts) {
    ob_start();
	if (wp_is_mobile()):
		$rand = random_int(10000, 99999);
	?>
<div id="gpt-passback-<?=$rand?>">
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
    googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Hp_Cube2', [[300, 250], [336, 280]], 'gpt-passback-<?=$rand?>').addService(googletag.pubads());
    googletag.enableServices();
    googletag.display('gpt-passback-<?=$rand?>');
    });
  </script>
</div>
<?php
	else:
?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- מודעת דף הבית לאתר החדש למעלה -->
<ins class="adsbygoogle"
     style="display:inline-block;height:90px;width: 100%;"
     data-ad-client="ca-pub-3724182673190727"
     data-ad-slot="1416244750"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
	endif;
?>

<?php
	return ob_get_clean();
}
*/
add_shortcode( 'pnt_frontpage_section_ad', 'pnt_frontpage_section_ad' );

function pnt_frontpage_above_sections_ad($atts) {
    ob_start();
?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- מודעת דף הבית לאתר החדש למעלה -->
<ins class="adsbygoogle"
     style="display:inline-block;height:90px;width: 100%;"
     data-ad-client="ca-pub-3724182673190727"
     data-ad-slot="1416244750"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
	return ob_get_clean();
}
/*
function pnt_frontpage_above_sections_ad($atts) {
    ob_start();
	if (wp_is_mobile()):
		$rand = random_int(10000, 99999);
	?>
<!-- /136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Hp_Cube -->
<div id='div-gpt-ad-1632999518084-0' style='min-width: 300px; min-height: 250px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1632999518084-0'); });
  </script>
</div>
<?php
	else:
?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- מודעת דף הבית לאתר החדש למעלה -->
<ins class="adsbygoogle"
     style="display:inline-block;height:90px;width: 100%;"
     data-ad-client="ca-pub-3724182673190727"
     data-ad-slot="1416244750"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
	endif;
?>

<?php
	return ob_get_clean();
}
*/
add_shortcode( 'pnt_frontpage_above_sections_ad', 'pnt_frontpage_above_sections_ad' );
?>
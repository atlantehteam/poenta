<?php

function pnt_header_scripts() {
?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DXMDC');</script>
<!-- End Google Tag Manager -->
<meta name="google-site-verification" content="OITAzjIWhLP2jAb3sPC86IJuY3Li4QJjdL4lHaYBiRo" />

<!-- Facebook Pixel Code -->
<script>
setTimeout(function(){
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '727257487431678');
  fbq('track', 'PageView');
}, 3000);
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=727257487431678&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->


<!-- Positive Mobile -->
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>

<?php /*
<script>
window.googletag = window.googletag || {cmd: []};
googletag.cmd.push(function() {
  var slot = googletag.defineOutOfPageSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Interstitial_Adx',
  googletag.enums.OutOfPageFormat.INTERSTITIAL);
  if (slot) slot.addService(googletag.pubads());
  googletag.enableServices();
  // NOTE! Consider delaying until first div on page
  googletag.display(slot);
});
</script>
*/ ?>

<?php /* if (wp_is_mobile()): ?>
<script>
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd.push(function() {
    googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Interstitial_Cube', [[360, 360], [300, 250], [336, 280]], 'div-gpt-ad-1632998926714-0').addService(googletag.pubads());
    googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Sticky_Banner', [[320, 100], [320, 50]], 'div-gpt-ad-1632999040149-0').addService(googletag.pubads());
    googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Hp_Cube', [[336, 280], [300, 250]], 'div-gpt-ad-1632999518084-0').addService(googletag.pubads());
    googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Hp_Cube2', [[300, 250], [336, 280]], 'div-gpt-ad-1632999835603-0').addService(googletag.pubads());
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<?php else: ?>
  <script>console.log('nooooooo')</script>
<?php endif; */ ?>
<!-- End Positive Mobile -->

<?php
} // end pnt_header_scripts


function pnt_header_banner() {
?>
<!-- מודעת דף הבית לאתר החדש למעלה -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
	style="display:inline-block;width:728px;height:90px"
	data-ad-client="ca-pub-3724182673190727"
	data-ad-slot="1416244750"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
}


/*function pnt_header_banner() {
  if (wp_is_mobile()): ?>
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<div id="gpt-passback-topbanner">
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
    googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Interstitial_Cube', [[300, 250], [336, 280], [360, 360]], 'gpt-passback-topbanner').addService(googletag.pubads());
    googletag.enableServices();
    googletag.display('gpt-passback-topbanner');
    });
  </script>
</div>
    <?php else: ?>
<!-- מודעת דף הבית לאתר החדש למעלה -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
	style="display:inline-block;width:728px;height:90px"
	data-ad-client="ca-pub-3724182673190727"
	data-ad-slot="1416244750"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
    <?php endif; ?>
  <?php
}
*/


function pnt_footer_scripts() {
  /*
  if (wp_is_mobile()): ?>
  <div class="pnt-sticky-footer">
    <!-- /136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Sticky_Banner -->
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
      <div id="gpt-passback-sticky">
        <script>
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
          googletag.defineSlot('/136431902,22057489345/Poenta/MobileWeb/Poenta_MobileWeb_Sticky_Banner', [[320, 100], [320, 50]], 'gpt-passback-sticky').addService(googletag.pubads());
          googletag.enableServices();
          googletag.display('gpt-passback-sticky');
          });
        </script>
      </div>
  </div>
        <?php else: ?>

        <?php endif; ?>
      <?php
*/
}

add_action('template_redirect', function() {
  if (!empty($_GET['pnt_test'])) {
    $currentDate = date('Y-m-d H:i:s');
    $color = wp_is_mobile() ? 'red' : 'green';
    $text = wp_is_mobile() ? 'Mobile' : 'Desktop';
    $headerRows = '';
    foreach (getallheaders() as $name => $value) {
      $headerRows .= "<tr><td style='white-space: nowrap;padding: 10px;'>$name</td><td>$value</td></tr>";
     }
    echo "<html><body style='background: $color; color: white; font-size: 70px'>
      $text<br/>
      Cached at: {$currentDate}<br/>
      Headers:<br/>
      <table style='font-size: 50px;'>
        <tr><th style='text-align:start;'>Name</th><th style='text-align:start;'>Value</th></tr>
        $headerRows
      </table>
    </body></html>";
    die;
  }
});
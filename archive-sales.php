
<?php get_header(); ?>
<div class="products_container">
<div class="products_area">
	<?php
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$sales = new WP_Query( array( 
			'post_type' => 'sales', 
			'posts_per_page' => -1,
            'orderby' => 'date',
            'paged' => $paged,
			'order' => 'DESC'
		));
	?>
	<?php
	if( $sales->have_posts() ){
        while( $sales->have_posts() ){ $sales->the_post(); 
            $image = get_the_post_thumbnail_url($post->ID); 
        ?>
			<a href="<?=the_permalink(); ?>" target="_blank" class="product">
				<div class="inner">
					<div class="image" ><img src="<?php echo $image; ?>" alt=""></div>
					<div class="title"><?php the_title(); ?></div>
					<div class="info">
						<div class="price"><?=the_field('price'); ?></div>
                        <div><button>מעבר לרכישה</button></div>
					</div>
				</div>
			</a>
		<?php }
	} else { ?>
		לא נמצאו המלצות במערכת, נראה שיש בעיה.
	<?php }
	wp_reset_postdata();
?>
<div class="pagination">
    <?php 
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<i></i> %1$s', __( '<', 'text-domain' ) ),
            'next_text'    => sprintf( '%1$s <i></i>', __( '>', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
        ) );
    ?>
</div>
</div>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
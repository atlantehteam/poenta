<?php 

/**

 * The template for displaying all default pages.

 *

 * @package Avantgardia

 * @subpackage Newspeak

 * @since Newspeak 1.0

 */

 

get_header(); ?>


				<?php avantgardia_single_post_media(); ?>
	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<div class="entry-content clearfix">

		<?php do_action( 'avantgardia_after_entry_content_open' ); ?>

		<?php 

			if ( have_posts() ) :

				

				the_post(); 

		?>				

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="single-post-content">

					<header>

						<?php

							the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); 						

						?>						

					</header>					

					<div class="entry-content clearfix">

						<?php the_content(); ?>

						<?php wp_link_pages(); ?>

					</div>

				</div>

			</article>

			<?php

			if ( comments_open() ) {

				comments_template();

			}

			?>

		<?php endif; ?>

		<?php do_action( 'avantgardia_after_entry_content_close' ); ?>

			</div>

		</main><!-- .site-main -->

	</div><!-- .content-area -->

	

<?php

 

get_sidebar(); 

get_footer(); 
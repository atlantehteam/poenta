(function($) {
    function injectEmbededCode() {
        setTimeout(() => {
            $('.embed-wrapper').each(function() {
                const $this = $(this);
                const src = $this.attr('data-src');
                $this.find('iframe').attr('src', src);
            })
        }, 1000);
    }
$(function() {
    injectEmbededCode();
})
})(jQuery);
